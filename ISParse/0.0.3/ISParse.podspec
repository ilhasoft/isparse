#
# Be sure to run `pod lib lint ISParse.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ISParse"
  s.version          = "0.0.3"
  s.summary          = "Easy iOS development"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                       Libs to improve iOS development
                       DESC

  s.homepage         = "https://bitbucket.org/ilhasoft/isparse"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Daniel" => "daniel@ilhasoft.com.br" }
  s.source       = { :git => "https://bitbucket.org/ilhasoft/isparse.git", :tag => "0.0.3" }
  s.social_media_url   = "https://twitter.com/danielamarall"

  s.ios.deployment_target = '8.0'

  s.source_files = 'ISParse/**/*'
  #s.resource_bundles = {
  #  'ISParse' => ['ISParse/Assets/*.png']
  #}

  #s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Parse'
  s.dependency 'ParseUI'
  s.dependency 'SwiftValidator', '3.0.3'
  s.dependency 'IlhasoftCore', '0.0.6'
end
