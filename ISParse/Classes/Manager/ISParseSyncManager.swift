//
//  CASyncManager.swift
//  CatalogoAmway
//
//  Created by Daniel Amaral on 06/09/16.
//  Copyright © 2016 Catalogo Amway. All rights reserved.
//

import UIKit
import Parse

public class ISParseSyncManager: NSObject {
    
    public static var parseQueries:[PFQuery] = []
    
    public class func syncDataWithCompletion(completion:() -> Void) {
        
        if parseQueries.isEmpty {
            print("parseQueries isEmpty")
            return
        }
        
        for (index,query) in parseQueries.enumerate() {
            
            query.limit = 1000
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                
                if error != nil {
                    print(error?.localizedDescription)
                }else {
                    if let objects = objects {
                        PFObject.pinAllInBackground(objects, block: { (success, error) in
                            if success {
                                if index + 1 == parseQueries.count {
                                    completion()
                                }
                            }else if error != nil {
                                print(error?.localizedDescription)
                            }
                            
                        })
                    }
                }
                
            })
            
        }
        
    }
    
}
