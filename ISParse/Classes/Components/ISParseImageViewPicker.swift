//
//  ISParseImageView.swift
//  ISParse
//
//  Created by Daniel Amaral on 15/05/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import Foundation
import IlhasoftCore

public class ISParseImageViewPicker: ISImageViewPicker, ISPersistable {
        
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    
    @IBInspectable public var required: Bool = false {
        didSet {
            
        }
    }
    
    @IBInspectable public var requiredError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldType: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldTypeError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }
    
}