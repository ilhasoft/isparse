//
//  ISParseImageScrollView.swift
//  Pods
//
//  Created by Daniel Amaral on 17/07/16.
//
//

import UIKit
import ISScrollViewPageSwift

public class ISParseImageScrollView: ISScrollViewPage, ISPresentable {

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }
        
}
