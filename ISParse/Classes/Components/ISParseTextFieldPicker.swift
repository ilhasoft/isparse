//
//  ISParseTextFieldPicker.swift
//  Pods
//
//  Created by Daniel Amaral on 26/06/16.
//
//

import UIKit
import IlhasoftCore
import Parse

public class ISParseTextFieldPicker: ISTextFieldAlamofirePicker, ISPersistable {

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
 
    @IBInspectable public var required: Bool = false {
        didSet {
            
        }
    }
    
    @IBInspectable public var requiredError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldType: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldTypeError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var dataSourceEntity: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var dataSourceField: String = "" {
        didSet {
            
        }
    }
    
    public func loadDataFromParse() {
        if !(dataSourceEntity.isEmpty && dataSourceField.isEmpty) {
            
            let query = PFQuery(className: dataSourceEntity)
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                if let objects = objects {
                    for object in objects {
                        self.dataArray.append(object.objectForKey(self.dataSourceField) as! String)
                    }
                }
            })
            
        }
    }
    
}
