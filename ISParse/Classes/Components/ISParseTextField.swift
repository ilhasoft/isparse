//
//  ISTextField.swift
//  ISParse
//
//  Created by Daniel Amaral on 30/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import Foundation
import UIKit

public class ISParseTextField: UITextField, ISPersistable {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable public var required: Bool = false {
        didSet {
            
        }
    }
    
    @IBInspectable public var requiredError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldType: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldTypeError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }

    
}