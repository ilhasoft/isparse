//
//  ISParseLabel.swift
//  Pods
//
//  Created by Daniel Amaral on 14/07/16.
//
//

import UIKit

public class ISParseLabel: UILabel, ISPresentable {

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }
    
}
