//
//  ISParseRadioButton.swift
//  ISParse
//
//  Created by Daniel Amaral on 18/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import DLRadioButton

public class ISParseRadioButton: DLRadioButton, ISPersistable {

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    @IBInspectable public var required: Bool = false {
        didSet {
            
        }
    }
    
    @IBInspectable public var requiredError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldType: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldTypeError: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var entityName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var fieldName: String = "" {
        didSet {
            
        }
    }
    
    @IBInspectable public var selectedFieldValue: String = "" {
        didSet {
            
        }
    }

    @IBInspectable override public var multipleSelectionEnabled: Bool {
        didSet {
            
        }
    }


}
