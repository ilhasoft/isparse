//
//  ISParseFieldType.swift
//  ISParse
//
//  Created by Daniel Amaral on 22/05/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

public enum ISParseFieldType: String {
    case Text = "Text"
    case CPF = "CPF"
    case Email = "Email"
    case Phone = "Phone"
    case CEP = "CEP"
    case Number = "Number"
    case Logic = "Logic"
    case Image = "Image"
    case File = "File"
    case Array = "Array"
}
