//
//  ISParseStringExtension.swift
//  Pods
//
//  Created by Daniel Amaral on 19/07/16.
//
//

import UIKit

extension String {

    public var capitalizeFirst: String {
        if isEmpty { return "" }
        var result = self
        result.replaceRange(startIndex...startIndex, with: String(self[startIndex]).uppercaseString)
        return result
    }
    
}
