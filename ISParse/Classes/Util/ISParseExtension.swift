//
//  ISParseExtension.swift
//  Pods
//
//  Created by Daniel Amaral on 08/09/16.
//
//

import Parse

extension Parse {

    public class func appIsOfflineMode() -> Bool {
        if let isOffLineMode = NSUserDefaults.standardUserDefaults().objectForKey("isOfflineMode") {
            return isOffLineMode as! Bool
        }else {
            return false
        }
    }
    
    public class func setAppIsOfflineMode(offlineMode:Bool) {
        NSUserDefaults.standardUserDefaults().setObject(offlineMode, forKey: "isOfflineMode")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}
