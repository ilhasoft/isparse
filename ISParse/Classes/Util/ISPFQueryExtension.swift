//
//  ParseExtension.swift
//  CatalogoAmway
//
//  Created by Daniel Amaral on 06/09/16.
//  Copyright © 2016 Catalogo Amway. All rights reserved.
//

import UIKit
import Parse

extension PFQuery {

    public func getDataFromLocalStoreElseSetupCachePolicy(cachePolicy:PFCachePolicy) {
        
        if Parse.appIsOfflineMode() == true {
            self.fromLocalDatastore()
        }else if !Parse.isLocalDatastoreEnabled() {
            self.cachePolicy = cachePolicy
        }
        
    }
    
}
