//
//  ISParseView.swift
//  Pods
//
//  Created by Daniel Amaral on 13/07/16.
//
//

import UIKit
import SwiftValidator
import Parse

public class ISParseView: UIView {

    @IBOutlet public var fields:[AnyObject]!
    
    @IBInspectable public var entity:String!
    public var parseObject:PFObject?
    
    public var mainFieldsDictionary:NSMutableDictionary!
    public var childEntitiesWithDictionaryFields:[[String:NSMutableDictionary]]!
    public var parseObjectList = [PFObject]()
    public var requiredParseImageViews:[UIImageView] = []
    public var requiredParseRadioButtons:[ISParseRadioButton] = []
    public var requiredParseSliders:[UISlider] = []
    public var requiredParseSwitchs:[UISwitch] = []
    public var requiredParseTextViews:[UITextView] = []
    public var entitiesToIncludeKey:NSMutableSet = []
    
    let validator = Validator()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        registerFields()
        setupFieldsWithData()
    }
    
    
    //MARK: Class Methods
    
    public func setupFieldsWithData() {
        
        if let data = self.parseObject {
            
            let query = PFQuery(className: self.entity)
            query.getDataFromLocalStoreElseSetupCachePolicy(.NetworkElseCache)
            
            for entity in self.entitiesToIncludeKey{
                query.includeKey((entity as! String))
            }
            
            query.getObjectInBackgroundWithId(data.objectId!, block: { (pObject, error) in
                
                if error != nil {
                    print(error)
                    return
                }
                
                if let pObject = pObject {
                    self.parseObject = pObject
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                        // do some task
                        dispatch_async(dispatch_get_main_queue()) {
                            self.fillFields(pObject)
                        }
                    }
                }
            })
            
        }
    }
    
    public func fillFields(pObject:PFObject) {
        if let fields = fields where !fields.isEmpty {
            for object in fields {
                
                if let textField = object as? ISPersistable where textField is UITextField {
                    
                    if  !(textField.entityName.characters.count > 0
                        && textField.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[textField.fieldName] where textField.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredFieldsArray = fields.filter({($0 as! ISPersistable).fieldName == textField.fieldName})
                                
                                if !filteredFieldsArray.isEmpty {
                                    let textFieldFromArray = filteredFieldsArray[index] as! UITextField
                                    textFieldFromArray.text = String(object)
                                }
                            }
                            
                        }else if !(value is NSNull){
                            (textField as! UITextField).text = value as? String
                        }
                    }else if let childObject = pObject[textField.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[textField.fieldName] {
                            if !(value is NSNull) {
                                (textField as! UITextField).text = String(value)
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                }else if let textView = object as? ISPersistable where textView is UITextView {
                    
                    if  !(textView.entityName.characters.count > 0
                        && textView.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[textView.fieldName] where textView.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredFieldsArray = fields.filter({($0 as! ISPersistable).fieldName == textView.fieldName})
                                
                                if !filteredFieldsArray.isEmpty {
                                    let textViewFromArray = filteredFieldsArray[index] as! UITextView
                                    textViewFromArray.text = String(object)
                                }
                            }
                            
                        }else if !(value is NSNull){
                            (textView as! UITextView).text = value as? String
                        }
                    }else if let childObject = pObject[textView.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[textView.fieldName] {
                            if !(value is NSNull) {
                                (textView as! UITextView).text = String(value)
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                }else if var imageView = object as? ISPersistable where imageView is UIImageView {
                    
                    if  !(imageView.entityName.characters.count > 0
                        && imageView.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[imageView.fieldName] where imageView.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredParseImageViewsArray = fields.filter({($0 as! ISPersistable).fieldName == imageView.fieldName})
                                
                                if !filteredParseImageViewsArray.isEmpty {
                                    let parseImageViewFromArray = filteredParseImageViewsArray[index] as! UIImageView
                                    parseImageViewFromArray.sd_setImageWithURL(NSURL(string: (object as! PFFile).url!))
                                }
                            }
                            
                        }else if !(value is NSNull) {
                            (imageView as! UIImageView).sd_setImageWithURL(NSURL(string: (value as! PFFile).url!))
                        }
                    }else if let childObject = pObject[imageView.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[imageView.fieldName] {
                            if !(value is NSNull) {
                                (imageView as! UIImageView).sd_setImageWithURL(NSURL(string: (value as! PFFile).url!))
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                    
                }else if let radioButton = object as? ISParseRadioButton {
                    
                    if  !(radioButton.entityName.characters.count > 0
                        && radioButton.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[radioButton.fieldName] where radioButton.entityName == pObject.parseClassName {
                        
                        let radioButtonList = fields.filter({$0 is ISParseRadioButton})
                        checkIfValueExistsInRadioButtonListAndMark(value, radioButtonList: radioButtonList as! [ISParseRadioButton], fieldName: radioButton.fieldName)
                        
                        
                    }else if let childObject = pObject[radioButton.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[radioButton.fieldName] {
                            
                            let radioButtonList = fields.filter({$0 is ISParseRadioButton})
                            checkIfValueExistsInRadioButtonListAndMark(value, radioButtonList: radioButtonList as! [ISParseRadioButton], fieldName: radioButton.fieldName)
                            
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                    
                }else if var slider = object as? ISPersistable where slider is UISlider  {
                    
                    if  !(slider.entityName.characters.count > 0
                        && slider.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[slider.fieldName] where slider.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredParseSliderArray = fields.filter({($0 as! ISPersistable).fieldName == slider.fieldName})
                                
                                if !filteredParseSliderArray.isEmpty {
                                    let parseSliderFromArray = filteredParseSliderArray[index] as! UISlider
                                    parseSliderFromArray.value = Float(object as! NSNumber)
                                }
                            }
                            
                        }else if !(value is NSNull) {
                            (slider as! UISlider).value = Float(value as! NSNumber)
                        }
                    }else if let childObject = pObject[slider.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[slider.fieldName] {
                            if !(value is NSNull) {
                                (slider as! UISlider).value = Float(value as! NSNumber)
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                }else if var switchComponent = object as? ISPersistable where switchComponent is UISwitch  {
                    
                    if  !(switchComponent.entityName.characters.count > 0
                        && switchComponent.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[switchComponent.fieldName] where switchComponent.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredParseSwitchArray = fields.filter({($0 as! ISPersistable).fieldName == switchComponent.fieldName})
                                
                                if !filteredParseSwitchArray.isEmpty {
                                    let parseSliderFromArray = filteredParseSwitchArray[index] as! UISwitch
                                    if let object = object as? Bool {
                                        parseSliderFromArray.setOn(object, animated: true)
                                    }else {
                                        print("\(switchComponent.fieldName) needs to be a boolean value")
                                    }
                                }
                            }
                            
                        }else if !(value is NSNull) {
                            
                            if let value = value as? Bool {
                                (switchComponent as! UISwitch).setOn(value, animated: true)
                            }else {
                                print("\(switchComponent.fieldName) needs to be a boolean value")
                            }
                            
                        }
                    }else if let childObject = pObject[switchComponent.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[switchComponent.fieldName] {
                            if !(value is NSNull) {
                                if let value = value as? Bool {
                                    (switchComponent as! UISwitch).setOn(value, animated: true)
                                }else {
                                    print("\(switchComponent.fieldName) needs to be a boolean value")
                                }
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                }else if var label = object as? ISPresentable where label is UILabel {
                    if  !(label.entityName.characters.count > 0
                        && label.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    if let value = pObject[label.fieldName] where label.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let filteredParseLabelArray = fields.filter({($0 as! ISPresentable).fieldName == label.fieldName &&
                                                                            ($0 as! ISPresentable).entityName == label.entityName})
                                
                                if !filteredParseLabelArray.isEmpty {
                                    let parseLabelFromArray = filteredParseLabelArray[index] as! UILabel
                                    parseLabelFromArray.text = String(object)
                                }
                            }
                            
                        }else if !(value is NSNull) {
                            (label as! UILabel).text = String(value)
                        }
                    }else if let childObject = pObject[label.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[label.fieldName] {
                            if !(value is NSNull) {
                                (label as! UILabel).text = String(value)
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                }else if var imageScrollView = object as? ISPresentable where imageScrollView is ISParseImageScrollView {
                    
                    if  !(imageScrollView.entityName.characters.count > 0
                        && imageScrollView.fieldName.characters.count > 0) {
                        continue
                    }
                    
                    var imageScrollView = (imageScrollView as! ISParseImageScrollView)
                    let width = imageScrollView.frame.size.width
                    let height = imageScrollView.frame.size.height
                    
                    if let value = pObject[imageScrollView.fieldName] where imageScrollView.entityName == pObject.parseClassName {
                        if let array = value as? NSArray {
                            
                            for (index, object) in array.enumerate() {
                                
                                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                                imageView.sd_setImageWithURL(NSURL(string: (object as! PFFile).url!))
                                imageView.contentMode = imageScrollView.contentMode
                                
                                imageScrollView.addCustomView(imageView)
                            }
                            
                        }else if !(value is NSNull) {
                            
                            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                            imageView.sd_setImageWithURL(NSURL(string: (value as! PFFile).url!))
                            imageView.contentMode = (imageScrollView as! ISParseImageScrollView).contentMode
                            
                            imageScrollView.addCustomView(imageView)
                        }
                    }else if let childObject = pObject[imageScrollView.entityName] as? PFObject {
                        //O campo é do tipo PFObject (relacionamento)
                        if let value = childObject[imageScrollView.fieldName] {
                            if !(value is NSNull) {
                                
                                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                                imageView.sd_setImageWithURL(NSURL(string: (value as! PFFile).url!))
                                
                                imageScrollView.addCustomView(imageView)
                            }
                        }
                    }else {
                        //O campo é inexistente no Parse
                        continue
                    }
                    
                    
                }
            }
        }
    }
    
    private func checkIfValueExistsInRadioButtonListAndMark(value:AnyObject, radioButtonList:[ISParseRadioButton], fieldName:String) {
        
        for radioButton in radioButtonList where radioButton.fieldName == fieldName{
            
            if value is NSNull {
                continue
            }
            
            let selectedFieldValue = radioButton.selectedFieldValue as AnyObject
            
            if let value = value as? Bool {
                if value == selectedFieldValue.boolValue {
                    radioButton.selected = true
                }
                
            }else if let value = value as? String {
                if value == selectedFieldValue as! String {
                    radioButton.selected = true
                }
            }else if let value = value as? NSNumber {
                if value == selectedFieldValue as! NSNumber {
                    radioButton.selected = true
                }
            }
        }
    }
    
    public func registerFields() {
        
        if let objects = fields where !objects.isEmpty {
            for object in objects {
                
                if let component = object as? ISPersistable {
                    if component.entityName.characters.count > 0 && component.entityName != self.entity {
                        self.entitiesToIncludeKey.addObject(component.entityName)
                    }
                }else if let component = object as? ISPresentable {
                    if component.entityName.characters.count > 0 && component.entityName != self.entity {
                        self.entitiesToIncludeKey.addObject(component.entityName)
                    }
                }
                
                if let textField = object as? ISPersistable where textField is UITextField {
                    
                    switch textField.fieldType {
                    case ISParseFieldType.Text.rawValue :
                        if textField.required == true {
                            validator.registerField(textField as! UITextField, rules: [RequiredRule(message: textField.requiredError)])
                        }
                        break
                    case ISParseFieldType.CPF.rawValue :
                        if textField.required == true {
                            validator.registerField(textField as! UITextField, rules: [RequiredRule(message: textField.requiredError)])
                        }
                        break
                    case ISParseFieldType.Email.rawValue :
                        if textField.required == true {
                            validator.registerField(textField as! UITextField, rules: [EmailRule(message: textField.fieldTypeError),RequiredRule(message: textField.requiredError)])
                        }
                        break
                    case ISParseFieldType.Phone.rawValue :
                        if textField.required == true {
                            validator.registerField(textField as! UITextField, rules: [RequiredRule(message: textField.requiredError)])
                        }
                        break
                    case ISParseFieldType.CEP.rawValue :
                        if textField.required == true {
                            validator.registerField(textField as! UITextField, rules: [RequiredRule(message: textField.requiredError)])
                        }
                        break
                    default:
                        break
                    }
                }else if let imageView = object as? ISPersistable where imageView is UIImageView {
                    
                    if imageView.required == true {
                        requiredParseImageViews.append(imageView as! UIImageView)
                    }
                }else if let radioButton = object as? ISParseRadioButton {
                    
                    if radioButton.required == true {
                        requiredParseRadioButtons.append(radioButton)
                    }
                }else if let slider = object as? ISPersistable where slider is UISlider {
                    
                    if slider.required == true {
                        requiredParseSliders.append(slider as! UISlider)
                    }
                }else if let switchComponent = object as? ISPersistable where switchComponent is UISwitch {
                    
                    if switchComponent.required == true {
                        requiredParseSwitchs.append(switchComponent as! UISwitch)
                    }
                }else if let textView = object as? ISPersistable where textView is UITextView {
                    
                    if textView.required == true {
                        requiredParseTextViews.append(textView as! UITextView)
                    }
                }
            }
        }
    }
    
    public func validateAndSaveWithCompletion(completion:(success:Bool,object:PFObject?) -> Void) {
        
        if self.entity == nil {
            print("ISViewController subclass need setup the entity attribute")
            completion(success: false, object: nil)
        }
        
        validator.validate { (errors) in
            for (_, error) in self.validator.errors {
                UIAlertView(title: nil, message: error.errorMessage, delegate: self, cancelButtonTitle: "OK").show()
                completion(success: false, object: nil)
                return
            }
            
            if !self.requiredParseImageViews.isEmpty {
                let filtered = self.requiredParseImageViews.filter({$0.image == nil})
                
                if !filtered.isEmpty {
                    UIAlertView(title: nil, message: (filtered[0] as! ISPersistable).requiredError, delegate: self, cancelButtonTitle: "OK").show()
                    completion(success: false, object: nil)
                    return
                }
            }
            
            if !self.requiredParseSliders.isEmpty {
                
                let filtered = self.requiredParseSliders.filter({$0.value == -1})
                
                if !filtered.isEmpty {
                    UIAlertView(title: nil, message: (filtered[0] as! ISPersistable).requiredError, delegate: self, cancelButtonTitle: "OK").show()
                    completion(success: false, object: nil)
                    return
                }
            }
            
            if !self.requiredParseTextViews.isEmpty {
                
                let filtered = self.requiredParseTextViews.filter({$0.text == ""})
                
                if !filtered.isEmpty {
                    UIAlertView(title: nil, message: (filtered[0] as! ISPersistable).requiredError, delegate: self, cancelButtonTitle: "OK").show()
                    completion(success: false, object: nil)
                    return
                }
            }
            
            if !self.requiredParseRadioButtons.isEmpty {
                
                let totalSelected = self.requiredParseRadioButtons[0].selectedButtons().count
                
                if totalSelected == 0 {
                    UIAlertView(title: nil, message: self.requiredParseRadioButtons[0].requiredError, delegate: self, cancelButtonTitle: "OK").show()
                    completion(success: false, object: nil)
                    return
                }
            }
            
            self.mainFieldsDictionary = NSMutableDictionary()
            self.childEntitiesWithDictionaryFields = []
            
            for object in self.fields {
                
                var entityName:String!
                var fieldName:String!
                var fieldValue:AnyObject!
                var fieldType:ISParseFieldType!
                
                if let imageView = object as? ISPersistable where imageView is UIImageView && imageView.entityName.characters.count > 0
                    && imageView.fieldName.characters.count > 0 {
                    
                    if (imageView as! UIImageView).image != nil {
                        fieldValue = (imageView as! UIImageView).image
                    }else if let parseObject = self.parseObject where parseObject.objectForKey(imageView.fieldName) != nil {
                        fieldValue = NSNull()
                    }else if let parseObject = self.parseObject where (parseObject[imageView.entityName] is PFObject) {
                        if let pfObject = parseObject[imageView.entityName] as? PFObject {
                            if pfObject[imageView.fieldName] != nil {
                                fieldValue = NSNull()
                            }
                        }
                    }else {
                        continue
                    }
                    
                    entityName = imageView.entityName
                    fieldName = imageView.fieldName
                    fieldType = ISParseFieldType(rawValue: imageView.fieldType)
                    
                }else if let textField = object as? ISPersistable where textField is UITextField && textField.entityName.characters.count > 0
                    && textField.fieldName.characters.count > 0 {
                    
                    
                    if (textField as! UITextField).text?.characters.count > 0 {
                        fieldValue = (textField as! UITextField).text
                    }else if let parseObject = self.parseObject where parseObject.objectForKey(textField.fieldName) != nil {
                        fieldValue = NSNull()
                    }else if let parseObject = self.parseObject where (parseObject[textField.entityName] is PFObject) {
                        if let pfObject = parseObject[textField.entityName] as? PFObject {
                            if pfObject[textField.fieldName] != nil || !(pfObject[textField.fieldName] is NSNull){
                                fieldValue = NSNull()
                            }
                        }
                    }else{
                        continue
                    }
                    
                    entityName = textField.entityName
                    fieldName = textField.fieldName
                    fieldType = ISParseFieldType(rawValue: textField.fieldType)
                    
                }else if let radioButton = object as? ISParseRadioButton where radioButton.entityName.characters.count > 0
                    && radioButton.fieldName.characters.count > 0 {
                    
                    if radioButton.multipleSelectionEnabled {
                        if radioButton.selectedFieldValue.characters.count > 0 {
                            fieldValue = radioButton.selected ? radioButton.selectedFieldValue : NSNull()
                        }else{
                            if !radioButton.selected {
                                continue
                            }else {
                                fieldValue = radioButton.selectedFieldValue
                            }
                        }
                    }else {
                        if !radioButton.selected {
                            continue
                        }else {
                            fieldValue = radioButton.selectedFieldValue
                        }
                    }
                    
                    entityName = radioButton.entityName
                    fieldName = radioButton.fieldName
                    fieldType = ISParseFieldType(rawValue: radioButton.fieldType)
                    
                }else if let slider = object as? ISPersistable where slider is UISlider && slider.entityName.characters.count > 0
                    && slider.fieldName.characters.count > 0 {
                    
                    if (slider as! UISlider).value != -1 {
                        fieldValue = (slider as! UISlider).value
                    }else if let parseObject = self.parseObject where parseObject.objectForKey(slider.fieldName) != nil {
                        fieldValue = NSNull()
                    }else if let parseObject = self.parseObject where (parseObject[slider.entityName] is PFObject) {
                        if let pfObject = parseObject[slider.entityName] as? PFObject {
                            if pfObject[slider.fieldName] != nil {
                                fieldValue = NSNull()
                            }
                        }
                    }else {
                        continue
                    }
                    
                    entityName = slider.entityName
                    fieldName = slider.fieldName
                    fieldType = ISParseFieldType(rawValue: slider.fieldType)
                    
                }else if let switchComponent = object as? ISPersistable where switchComponent is UISwitch && switchComponent.entityName.characters.count > 0
                    && switchComponent.fieldName.characters.count > 0 {
                    
                    fieldValue = (switchComponent as! UISwitch).on
                    
                    entityName = switchComponent.entityName
                    fieldName = switchComponent.fieldName
                    fieldType = ISParseFieldType(rawValue: switchComponent.fieldType)
                    
                }else if let textView = object as? ISPersistable where textView is UITextView && textView.entityName.characters.count > 0
                    && textView.fieldName.characters.count > 0 {
                    
                    if (textView as! UITextView).text?.characters.count > 0 {
                        fieldValue = (textView as! UITextView).text
                    }else if let parseObject = self.parseObject where parseObject.objectForKey(textView.fieldName) != nil {
                        fieldValue = NSNull()
                    }else if let parseObject = self.parseObject where (parseObject[textView.entityName] is PFObject) {
                        if let pfObject = parseObject[textView.entityName] as? PFObject {
                            if pfObject[textView.fieldName] != nil || !(pfObject[textView.fieldName] is NSNull){
                                fieldValue = NSNull()
                            }
                        }
                    }else{
                        continue
                    }
                    
                    entityName = textView.entityName
                    fieldName = textView.fieldName
                    fieldType = ISParseFieldType(rawValue: textView.fieldType)
                    
                }else{
                    continue
                }
                
                self.buildDictionaryDataStructure(entityName, fieldName: fieldName, fieldValue: fieldValue, fieldType:fieldType)
                
            }
            
            self.buildParseStructureAndSave({ (success, object) in
                completion(success: success, object: object)
            })
        }
        
    }
    
    private func buildParseStructureAndSave(completion:(success:Bool,object:PFObject?) -> Void) {
        if self.mainFieldsDictionary.count > 0 {
            if !self.childEntitiesWithDictionaryFields.isEmpty {
                
                self.childEntitiesWithDictionaryFields = self.childEntitiesWithDictionaryFields.sort({ $0.keys.first < $1.keys.first })
                
                for entities in self.childEntitiesWithDictionaryFields {
                    
                    let entityName = entities.keys.first!
                    let dictionary = NSMutableDictionary()
                    
                    for (key,value) in entities.values.first! {
                        dictionary.setObject(value, forKey: key as! NSCopying)
                    }
                    
                    let object = PFObject(className: entityName, dictionary: dictionary.mutableCopy() as? [String : AnyObject])
                    
                    if let parseObject = parseObject {
                        if let pfObject = parseObject[entityName] {
                            object.objectId = pfObject.objectId
                        }
                    }
                    
                    self.parseObjectList.append(object)
                    
                }
                
                PFObject.saveAllInBackground(self.parseObjectList, block: { (success, error) in
                    
                    for object in self.parseObjectList {
                        self.mainFieldsDictionary.setObject(object, forKey: object.parseClassName)
                    }
                    
                    self.parseObjectList = []
                    
                    self.saveMainEntityWithCompletion({ (success, object) in
                        completion(success: success, object: object)
                    })
                    
                })
                
            }else{
                self.saveMainEntityWithCompletion({ (success, object) in
                    completion(success: success, object: object)
                })
            }
            
        }
    }
    
    private func getFieldWithCast(fieldValue:AnyObject) -> AnyObject{
        
        let value = fieldValue
        
        if let value = value as? UIImage {
            return PFFile(name: "image",data: UIImageJPEGRepresentation(value, 0.3)!)!
        }else {
            return value
        }
    }
    
    private func buildDictionaryDataStructure(entityName:String,fieldName:String,fieldValue:AnyObject, fieldType:ISParseFieldType) {
        
        var fieldValue = fieldValue
        
        if !(fieldValue is NSNull) {
            switch fieldType {
            case .Number:
                fieldValue = fieldValue.integerValue
                break
            case .Logic:
                fieldValue = fieldValue.boolValue
                break
            case .Image:
                if let value = getFieldWithCast(fieldValue) as? PFFile {
                    fieldValue = value
                }else{
                    print("FieldType Image but the value is not a Image")
                }
                break
            case .Array:
                fieldValue = [getFieldWithCast(fieldValue)]
                break
            default:
                break
            }
        }
        
        if entityName != self.entity {
            
            let filtered = self.childEntitiesWithDictionaryFields.filter({
                
                let contains = $0.keys.contains(entityName)
                
                if contains {
                    var dictionary = $0.values.first
                    dictionary = self.buildArrayFieldIfNeeded(dictionary!, fieldName: fieldName, fieldValue: fieldValue, fieldType: fieldType)
                }
                
                return contains
            })
            
            if filtered.isEmpty {
                self.childEntitiesWithDictionaryFields.append([entityName:[fieldName:fieldValue]])
            }
            
        }else {
            self.mainFieldsDictionary = self.buildArrayFieldIfNeeded(self.mainFieldsDictionary, fieldName: fieldName, fieldValue: fieldValue, fieldType: fieldType)
        }
    }
    
    private func buildArrayFieldIfNeeded(dictionary:NSMutableDictionary,fieldName:String,fieldValue:AnyObject, fieldType: ISParseFieldType) -> NSMutableDictionary {
        
        let dictionary = dictionary
        
        if let value = dictionary.objectForKey(fieldName) {
            if var array = value as? NSArray {
                if !(fieldValue is NSNull) {
                    array = array.arrayByAddingObject(fieldValue[0])
                }
                dictionary.setObject(array, forKey: fieldName)
            }else{
                dictionary.setObject([value,fieldValue], forKey: fieldName)
            }
        }else if fieldValue is NSNull && fieldType == .Array {
            dictionary.setObject([], forKey: fieldName)
        }else if !(fieldValue is NSNull) {
            dictionary.setObject(fieldValue, forKey: fieldName)
        }else {
            dictionary.setObject(NSNull(), forKey: fieldName)
        }
        return dictionary
    }
    
    private func saveMainEntityWithCompletion(completion:(success:Bool,object:PFObject?) -> Void) {
        let object = PFObject(className: self.entity, dictionary: self.mainFieldsDictionary.mutableCopy() as? [String : AnyObject])
        
        if let data = self.parseObject {
            object.objectId = data.objectId
        }
        
        object.saveInBackgroundWithBlock({ (success, error) in
            if error != nil {
                print(error?.localizedDescription)
                completion(success: success, object: nil)
            }else{
                completion(success: success, object: object)
            }
            
        })
    }


}
