//
//  ISTableViewCell.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 09/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import Parse

public class ISTableViewCell: UITableViewCell {

    @IBOutlet public var viewParse:ISParseView!
    public var object:PFObject!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: Class Methods
    
    public func setupCellWithData(object:PFObject, atIndexPath indexPath: NSIndexPath) {
        
        self.object = object
        
        if viewParse != nil {
            viewParse.parseObject = object
            viewParse.registerFields()
            viewParse.setupFieldsWithData()
        }
    }
    
}
