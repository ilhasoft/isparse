//
//  ISExpandableTableViewController.swift
//  ISParse
//
//  Created by Dielson Sales on 30/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import Parse
import ParseUI

public protocol ISExpandableTableViewDelegate {
    func shouldExpandSectionAt(section: Int) -> Bool
}

public class ISExpandableTableViewController: PFQueryTableViewController {

    public var expandableTableViewDelegate: ISExpandableTableViewDelegate?

    var headerCellNibName: String!
    var expandedCellNibNames: [String] = []
    var selectedIndexPath: NSIndexPath?
    public var query: PFQuery?

    public init(parseClassName: String, headerCellNibName: String, expandedCellNibNames: [String], pullToRefreshEnabled: Bool, paginationEnabled:Bool, objectsPerPage:Int, lazyLoad: Bool) {
        super.init(style: UITableViewStyle.Plain, className: parseClassName)
        self.parseClassName = parseClassName
        self.headerCellNibName = headerCellNibName
        self.expandedCellNibNames.appendContentsOf(expandedCellNibNames)
        self.pullToRefreshEnabled = pullToRefreshEnabled
        self.paginationEnabled = paginationEnabled
        self.objectsPerPage = UInt(objectsPerPage)

        if lazyLoad {
            self.query = PFQuery()
        }
    }

    public init(parseClassName: String, headerCellNibName: String, expandedCellNibNames: [String], pullToRefreshEnabled: Bool, paginationEnabled: Bool, objectsPerPage: Int, query: PFQuery) {
        super.init(style: UITableViewStyle.Plain, className: parseClassName)
        self.parseClassName = parseClassName
        self.headerCellNibName = headerCellNibName
        self.expandedCellNibNames.appendContentsOf(expandedCellNibNames)
        self.pullToRefreshEnabled = pullToRefreshEnabled
        self.paginationEnabled = paginationEnabled
        self.objectsPerPage = UInt(objectsPerPage)
        self.query = query
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        registerNibs()
    }

    // MARK: Class methods

    private func registerNibs() {
        self.tableView.registerNib(UINib(nibName: headerCellNibName, bundle: nil), forCellReuseIdentifier: headerCellNibName)
        for selectedCellNibName in expandedCellNibNames {
            self.tableView.registerNib(UINib(nibName: selectedCellNibName, bundle: nil), forCellReuseIdentifier: selectedCellNibName)
        }
    }

    override public func queryForTable() -> PFQuery {
        if let query = self.query {
            return query
        }else{
            return PFQuery(className: self.parseClassName!)
        }
    }

    public func loadDataWithQuery(query:PFQuery) {
        self.query = query
        loadObjects()
    }

    public func expandSection(section: Int) {
        if let selectedIndexPath = self.selectedIndexPath {
            self.tableView(self.tableView, didSelectRowAtIndexPath: selectedIndexPath)
        }
        let indexPath = NSIndexPath(forRow: 0, inSection: section)
        self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None)
        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
    }


    public func retractSection(section: Int) {
        let indexPath = NSIndexPath(forRow: 0, inSection: section)
        if let selectedIndexPath = self.selectedIndexPath {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.tableView(self.tableView, didDeselectRowAtIndexPath: indexPath)
        }
    }


    // MARK: UITableViewDataSource

    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let objects = self.objects {
            return objects.count
        }
        return 0
    }

    override public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let selectedIndexPath = self.selectedIndexPath {
            if selectedIndexPath.section == section {
                return expandedCellNibNames.count + 1
            }
        }
        return 1
    }

    override public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var cellHeight: CGFloat = CGFloat(0.0)
        if let selectedIndexPath = self.selectedIndexPath {
            if selectedIndexPath == indexPath {
                if indexPath.row == 0 {
                    cellHeight = NSBundle.mainBundle().loadNibNamed(headerCellNibName, owner: self, options: nil)[0].bounds.size.height
                }else {
                    cellHeight = NSBundle.mainBundle().loadNibNamed(expandedCellNibNames[indexPath.row], owner: self, options: nil)[0].bounds.size.height
                }
            } else {
                cellHeight = NSBundle.mainBundle().loadNibNamed(headerCellNibName, owner: self, options: nil)[0].bounds.size.height
            }
        } else {
            cellHeight = NSBundle.mainBundle().loadNibNamed(headerCellNibName, owner: self, options: nil)[0].bounds.size.height
        }
        return cellHeight
    }

    override public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: ISTableViewCell!
        if let selectedIndexPath = self.selectedIndexPath {
            if selectedIndexPath.section == indexPath.section {
                if indexPath.row == 0 {
                    cell = tableView.dequeueReusableCellWithIdentifier(headerCellNibName, forIndexPath: indexPath) as! ISTableViewCell
                    cell.setupCellWithData(self.objects![indexPath.section], atIndexPath: indexPath)
                    setupHeaderCell(cell, atIndexPath: indexPath)
                } else {
                    cell = tableView.dequeueReusableCellWithIdentifier(expandedCellNibNames[indexPath.row - 1], forIndexPath: indexPath) as! ISTableViewCell
                    cell.setupCellWithData(self.objects![indexPath.section], atIndexPath: indexPath)
                    setupExpandedCell(cell, atIndexPath: indexPath)
                }
            } else {
                cell = tableView.dequeueReusableCellWithIdentifier(headerCellNibName, forIndexPath: indexPath) as! ISTableViewCell
                cell.setupCellWithData(self.objects![indexPath.section], atIndexPath: indexPath)
                setupHeaderCell(cell, atIndexPath: indexPath)
            }
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier(headerCellNibName, forIndexPath: indexPath) as! ISTableViewCell
            cell.setupCellWithData(self.objects![indexPath.section], atIndexPath: indexPath)
            setupHeaderCell(cell, atIndexPath: indexPath)
        }
        return cell
    }

    public func setupHeaderCell(cell: ISTableViewCell, atIndexPath indexPath: NSIndexPath) {
    }

    public func setupExpandedCell(cell: ISTableViewCell, atIndexPath indexPath: NSIndexPath) {
    }

    // MARK: UITableViewCellDelegate

    override public func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.row == 0 {
            return indexPath
        }
        return nil
    }

    override public func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.row == 0 {
            return indexPath
        }
        return nil
    }

    override public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        // Prevents expanding the section if it shouldn't
        if let delegate = expandableTableViewDelegate {
            if delegate.shouldExpandSectionAt(indexPath.section) == false {
                tableView.deselectRowAtIndexPath(indexPath, animated: false)
                return
            }
        }
        if let selectedIndexPath = self.selectedIndexPath {
            // If there's another selected row, deselect it instead
            guard selectedIndexPath.section != indexPath.section else {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
                self.tableView(tableView, didDeselectRowAtIndexPath: indexPath)
                return
            }
        }
        self.selectedIndexPath = indexPath
        var indexPathsToInsert: [NSIndexPath] = []
        for index in 0..<expandedCellNibNames.count {
            let cellIndexPath = NSIndexPath(forRow: index + 1, inSection: indexPath.section)
            indexPathsToInsert.append(cellIndexPath)
        }
        tableView.beginUpdates()
        tableView.insertRowsAtIndexPaths(indexPathsToInsert, withRowAnimation: UITableViewRowAnimation.Automatic)
        tableView.endUpdates()
    }

    override public func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndexPath = nil
        var indexPathsToRemove: [NSIndexPath] = []
        for index in 0..<expandedCellNibNames.count {
            let cellIndexPath = NSIndexPath(forRow: index + 1, inSection: indexPath.section)
            indexPathsToRemove.append(cellIndexPath)
        }
        tableView.beginUpdates()
        tableView.deleteRowsAtIndexPaths(indexPathsToRemove, withRowAnimation: UITableViewRowAnimation.Automatic)
        tableView.endUpdates()
    }

}
