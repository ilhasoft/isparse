//
//  ISTableViewController.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 09/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import Parse
import ParseUI

public protocol ISTableViewControllerDelegate {
    func didSelectRowAtIndex(tableView:UITableView,index:Int,data:PFObject)
}

public class ISTableViewController: PFQueryTableViewController {

    var tableViewCellNibName:String!
    public var query: PFQuery?
    var tableViewCell:ISTableViewCell!

    public var delegate:ISTableViewControllerDelegate?

    public init(parseClassName: String, tableViewCellNibName: String, pullToRefreshEnabled:Bool, paginationEnabled:Bool, objectsPerPage:Int, lazyLoad: Bool) {
        super.init(style: UITableViewStyle.Plain, className: parseClassName)
        self.parseClassName = parseClassName
        self.tableViewCellNibName = tableViewCellNibName
        self.pullToRefreshEnabled = pullToRefreshEnabled
        self.paginationEnabled = paginationEnabled
        self.objectsPerPage = UInt(objectsPerPage)

        if lazyLoad {
            self.query = PFQuery()
        }
    }

    public init(parseClassName:String,tableViewCellNibName:String,pullToRefreshEnabled:Bool, paginationEnabled:Bool, objectsPerPage:Int, query:PFQuery) {
        super.init(style: UITableViewStyle.Plain, className: parseClassName)
        self.query = query
        self.parseClassName = parseClassName
        self.tableViewCellNibName = tableViewCellNibName
        self.pullToRefreshEnabled = pullToRefreshEnabled
        self.paginationEnabled = paginationEnabled
        self.objectsPerPage = UInt(objectsPerPage)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        registerNib()    }

    //MARK: PFQueryTableViewController Methods

    override public func queryForTable() -> PFQuery {
        if let query = query {
            return query
        } else {
            return super.queryForTable()
        }
    }

    //MARK: Class Methods

    public func loadDataWithQuery(query:PFQuery) {
        self.query = query
        loadObjects()
    }

    public func setupTableView(backgroundColor:UIColor?, contentInset:UIEdgeInsets?, separatorColor:UIColor?) {
        self.tableView.backgroundColor = backgroundColor ?? self.tableView.backgroundColor
        self.tableView.contentInset = contentInset ?? self.tableView.contentInset
        self.tableView.separatorColor = separatorColor ?? self.tableView.separatorColor
    }

    private func registerNib() {
        self.tableView.registerNib(UINib(nibName: self.tableViewCellNibName, bundle: nil), forCellReuseIdentifier: NSStringFromClass(ISTableViewCell.self))
    }

    // MARK: - Table view data source

    override public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < objects!.count {
            if let delegate = delegate {
                delegate.didSelectRowAtIndex(self.tableView, index: indexPath.row, data: self.objects![indexPath.row])
            }
        } else {
            super.tableView(tableView, didSelectRowAtIndexPath: indexPath)
        }

    }

    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return NSBundle.mainBundle().loadNibNamed(self.tableViewCellNibName, owner: self, options: nil)[0].bounds.size.height
    }

    override public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row < objects!.count {
            let cell = tableView.dequeueReusableCellWithIdentifier(NSStringFromClass(ISTableViewCell.self), forIndexPath: indexPath) as! ISTableViewCell
            cell.setupCellWithData(self.objects![indexPath.row], atIndexPath: indexPath)
            setupCell(cell, atIndexPath: indexPath)
            return cell
        } else {
            return super.tableView(tableView, cellForRowAtIndexPath: indexPath)
        }
    }

    public func setupCell(cell: ISTableViewCell, atIndexPath indexPath: NSIndexPath) {
    }
}
