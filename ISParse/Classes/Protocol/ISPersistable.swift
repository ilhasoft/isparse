//
//  ISPersistable.swift
//  Pods
//
//  Created by Daniel Amaral on 30/06/16.
//
//

public protocol ISPersistable {
    
    var required: Bool { get set }
    var requiredError: String { get set }
    var fieldType: String { get set }
    var fieldTypeError: String { get set }
    var entityName: String { get set }
    var fieldName: String { get set }
    
}
