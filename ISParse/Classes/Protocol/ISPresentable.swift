//
//  ISPresentable.swift
//  Pods
//
//  Created by Daniel Amaral on 14/07/16.
//
//

import UIKit

public protocol ISPresentable {
    
    var entityName: String { get set }
    var fieldName: String { get set }

}
