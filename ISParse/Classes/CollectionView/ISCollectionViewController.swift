//
//  ISCollectionViewController.swift
//  Pods
//
//  Created by Daniel Amaral on 12/07/16.
//
//

import UIKit
import Parse
import MBProgressHUD

public protocol ISCollectionViewControllerDelegate {
    func didSelectItemAtIndex(collectionView:UICollectionView,index:Int,data:PFObject)
}

public class ISCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var dataList = [PFObject]()
    
    var parseClassName:String!
    var collectionViewCellNibName:String!
    var numberOfColumns:Int!
    var query:PFQuery?
    
    var delegate:ISCollectionViewControllerDelegate?
    
    static var collectionViewFlowLayout:UICollectionViewLayout {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.minimumLineSpacing = 1.0
        collectionViewFlowLayout.minimumInteritemSpacing = 1.0
        collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirection.Vertical;
        return collectionViewFlowLayout
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    public init(parseClassName: String, collectionViewCellNibName: String, numberOfColumns:Int = 1, query:PFQuery? = nil) {
        super.init(collectionViewLayout: ISCollectionViewController.collectionViewFlowLayout)
        self.parseClassName = parseClassName
        self.collectionViewCellNibName = collectionViewCellNibName
        self.numberOfColumns = numberOfColumns
        self.query = query
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        registerNib()
        loadData()
    }
    
    // MARK: UICollectionViewDataSource
    
    override public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }
    
    override public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row < self.dataList.count {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NSStringFromClass(ISCollectionViewCell.self), forIndexPath: indexPath) as! ISCollectionViewCell
            setupCell(cell, atIndexPath: indexPath)
            cell.setupCellWithData(self.dataList[indexPath.item], atIndexPath: indexPath)
            return cell
        } else {
            return super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        }
    }
    
    override public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       let object = self.dataList[indexPath.item] as! PFObject
        delegate?.didSelectItemAtIndex(collectionView,index:indexPath.item,data:object)
    }
    
    override public func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.collectionView?.performBatchUpdates(nil, completion: nil)
    }
    
    //MARK: Class Methods
    
    public func setupCell(cell: ISCollectionViewCell, atIndexPath indexPath: NSIndexPath) {
        
    }
    
    public func loadData() {
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let query = self.query ?? PFQuery(className: self.parseClassName)                
        
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            if let objects = objects {
                self.dataList = objects
            }
            
            self.collectionView!.reloadData()
        }
        
    }
    
    private func registerNib() {
        self.collectionView!.registerNib(UINib(nibName: self.collectionViewCellNibName, bundle: nil), forCellWithReuseIdentifier: NSStringFromClass(ISCollectionViewCell.self))
    }
    
    func getCellItemSize() -> CGSize {
        
        let cell = NSBundle.mainBundle().loadNibNamed(self.collectionViewCellNibName, owner: self, options: nil)[0]
        
        let height = cell.bounds.size.height
        
        let itemWidth = (CGRectGetWidth(self.collectionView!.frame) - (CGFloat(self.numberOfColumns) - 1)) / CGFloat(self.numberOfColumns)
        return CGSizeMake(itemWidth, height)
        
    }
    
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return getCellItemSize()
    }
    


}
