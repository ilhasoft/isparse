//
//  ISCollectionViewCell.swift
//  Pods
//
//  Created by Daniel Amaral on 12/07/16.
//
//

import UIKit
import Parse

public class ISCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet public var viewParse:ISParseView!
    public var object:PFObject!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: Class Methods
    
    public func setupCellWithData(object:PFObject, atIndexPath indexPath: NSIndexPath) {
        self.object = object
        
        if viewParse != nil {
            viewParse.parseObject = object
            viewParse.registerFields()
            viewParse.setupFieldsWithData()
        }
        
    }
    
}
