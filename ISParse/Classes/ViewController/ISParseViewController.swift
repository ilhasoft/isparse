//
//  ISViewController.swift
//  ISParse
//
//  Created by Daniel Amaral on 30/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import SwiftValidator

public class ISParseViewController: UIViewController {
    
    @IBOutlet public var parseView:ISParseView!
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.parseView.registerFields()
        self.parseView.setupFieldsWithData()
    }


   }
