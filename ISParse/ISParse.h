//
//  ISParse.h
//  ISParse
//
//  Created by Daniel Amaral on 25/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ISParse.
FOUNDATION_EXPORT double ISParseVersionNumber;

//! Project version string for ISParse.
FOUNDATION_EXPORT const unsigned char ISParseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ISParse/PublicHeader.h>


@import Parse;
@import ParseUI;
@import Bolts;